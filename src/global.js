class Global {
    getUrl() {
        return process.env.REACT_APP_API_URL || "http://localhost:4000/api";
    }
    getLastFmUrl() {
        return process.env.REACT_APP_LASTFM_URL || "https://ws.audioscrobbler.com/2.0/"
    }
}

export default new Global();