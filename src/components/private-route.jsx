import React from 'react';
import { BrowserRouter as Link, Route, Redirect } from "react-router-dom";

import AuthService from './../services/auth-service';

export default function PrivateRoute({children, ...rest}) {
    return (
        <Route
            {...rest}
            render={({ location }) => 
                AuthService.getUser() ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: "/login",
                            state: {from: location}
                        }}
                    />
                )
            }
        />
    );
}